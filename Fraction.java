package t1;

public class Fraction
{
	private int numerator;// 分子
	private int denominator;// 分母
	private String fraction;// 分数

	public Fraction()
	{
		super();
	}

	public Fraction(int numerator, int denominator)
	{
		this.numerator = numerator;
		this.denominator = denominator;
		// makeItProper();
	}

	public int getNumerator()
	{
		return numerator;
	}

	public void setNumerator(int numerator)
	{
		this.numerator = numerator;
	}

	public int getDenominator()
	{
		return denominator;
	}

	public void setDenominator(int denominator)
	{
		this.denominator = denominator;
	}

	public String getFraction()
	{
		return fraction;
	}

	public void setFraction(String fraction)
	{
		this.fraction = fraction;
	}

	public String creatfraction()
	{
		numerator = (int) (Math.random() * 10 + 2);// [0~1)之间的随机数
		denominator = (int) (Math.random() * 10 + 2);
		int i = numerator;
		int j = denominator;
		j = GCD(i, j);
		numerator = numerator / j;
		denominator = denominator / j;
		fraction = Reduction(numerator, denominator);
		return fraction;
	}

	public static int GCD(int m, int n)
	{ // 求最大公约数
		try
		{
			while (m % n != 0)
			{
				int t = m % n;
				m = n;
				n = t;
			}
		} catch (Exception e)
		{
			System.out.println(e);
		}
		return n;
	}

	public static Fraction transferToFraction(String s)
	{
		Fraction f = new Fraction();
		String[] num = s.split("/");
		if (num.length == 1)
		{
			f.setNumerator(Integer.parseInt(s));
			f.setDenominator(1);
			f.setFraction(s);
		} else
		{
			f.setNumerator(Integer.parseInt(num[0]));
			f.setDenominator(Integer.parseInt(num[1]));
			f.setFraction(s);
		}

		return f;

	}

	public static String Reduction(int m, int n)
	{
		String t;
		if (n == 1)
		{
			t = m + "";
		} else
		{
			t = m + "" + "/" + n + "";
		}
		return t;
	}
}
