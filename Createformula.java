package t1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Createformula
{
	public static int testNum = 5;
	private String[] result = new String[testNum];
	private String[] number1 = new String[testNum];
	private String[] number2 = new String[testNum];
	private String[] number3 = new String[testNum];
	private String[][] operator1 = new String[testNum][2];
	private int[][] prior = new int[testNum][2];
	private ArrayList<String> scoresList = new ArrayList<String>();
	private BufferedWriter writer;

	public Createformula()
	{

	}

	public void createTest()
	{
		String s1;
		Fraction f1 = new Fraction();
		Fraction f2 = new Fraction();
		Fraction f3 = new Fraction();
		for (int i = 0; i < testNum; i++)
		{
			f1.creatfraction();
			f2.creatfraction();
			f3.creatfraction();
			number1[i] = f1.getFraction();
			number2[i] = f2.getFraction();
			number3[i] = f3.getFraction();
			Createformula cf = new Createformula();
			operator1[i][0] = cf.randomOperator();
			operator1[i][1] = cf.randomOperator();
			for (int j = 0; j < 2; j++)
			{
				if (operator1[i][j].equals("+") || operator1[i][j].equals("-"))
				{
					prior[i][j] = 0;
				} else
				{
					prior[i][j] = 1;
				}

			}
			if (prior[i][0] >= prior[i][1])
			{
				s1 = cf.stepAnswer(f1, f2, operator1[i][0]);
				result[i] = cf.stepAnswer(Fraction.transferToFraction(s1), f3, operator1[i][1]);
			} else
			{
				s1 = cf.stepAnswer(f2, f3, operator1[i][1]);
				result[i] = cf.stepAnswer(Fraction.transferToFraction(s1), f1, operator1[i][0]);
			}

		}
	}

	// 校验答案
	public String[] checkAnswer(String[] answers)
	{
		for (int i = 0; i < testNum; i++)
		{
			if (!result[i].equals(answers[i]))
			{
				scoresList.add(new Integer(i + 1).toString());
			}
		}
		String[] str = new String[scoresList.size()];
		scoresList.toArray(str);
		return str;
	}

	public String[] getQuestions()
	{
		String[] questions = new String[testNum];
		for (int i = 0; i < testNum; i++)
			questions[i] = number1[i] + " " + operator1[i][0] + " " + number2[i] + " "
					+ operator1[i][1] + " " + number3[i] + "  =  ";
		return questions;
	}

	public String randomOperator()
	{
		String operator = null;
		Random random = new Random();
		switch (random.nextInt(4))
		{
		case 0:
			operator = "+";
			break;
		case 1:
			operator = "-";
			break;
		case 2:
			operator = "x";
			break;
		case 3:
			operator = "÷";
			break;

		}

		return operator;
	}

	public String stepAnswer(Fraction f1, Fraction f2, String operator)
	{
		String s = null;
		switch (operator)
		{
		case "+":
			s = Calculator.add(f1, f2);
			break;
		case "-":
			s = Calculator.sub(f1, f2);
			break;
		case "x":
			s = Calculator.mul(f1, f2);
			break;
		case "÷":
			s = Calculator.div(f1, f2);
			break;
		}
		return s;
	}

	public String[] getStandardAnswer()
	{
		return result;
	}

}