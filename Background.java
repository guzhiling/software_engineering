package t1;

import javax.swing.*;

import t1.Createformula;

import java.awt.*;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.event.WindowStateListener;
import java.io.*;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class Background extends JFrame implements ActionListener
{
	private static final long serialVersionUID = 1L;
	private JLabel labTime;
	private JPanel jpMain;
	private JPanel jpTest;
	private JPanel jpAnswer;
	private Createformula background;
	private Integer minutes = 0, seconds = 0;
	private String minStr, secStr;
	private BufferedReader reader;
	private JButton btnSubmit, btnExit;
	private ArrayList<String> tips = new ArrayList<String>();
	private String[] questions = new String[Createformula.testNum];
	private JPanel[] jpQuestions = new JPanel[Createformula.testNum];
	private JLabel[] labQuestions = new JLabel[Createformula.testNum];
	private JTextField[] tfdAnswer = new JTextField[Createformula.testNum];
	private String[] answers = new String[Createformula.testNum];
	private String[] wrong;
	private boolean isEnd = false;

	// 客户端构造器
	public Background()
	{
		try
		{
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e)
		{
			e.printStackTrace();
		}
		background = new Createformula();
		background.createTest();
		questions = background.getQuestions();
		this.setLanguage();
		createComponent();
		this.setTitle(tips.get(0));
		this.setSize(800, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	// 创建面板
	public void createComponent()
	{
		jpMain = new JPanel();
		jpMain.setBackground(Color.lightGray);
		jpMain.setLayout(null);
		showTime();
		btnSubmit = new JButton(tips.get(3));
		btnSubmit.setBounds(250, 500, 80, 30);
		btnSubmit.addActionListener(this);
		jpMain.add(btnSubmit);
		btnExit = new JButton(tips.get(4));
		btnExit.setBounds(450, 500, 80, 30);
		btnExit.addActionListener(this);
		jpMain.add(btnExit);
		jpTest = new JPanel();
		jpTest.setLayout(new GridLayout(Createformula.testNum, 1, 20, 20));
		jpTest.setBackground(Color.white);
		for (int i = 0; i < Createformula.testNum; i++)
		{
			jpQuestions[i] = new JPanel();
			jpQuestions[i].setBackground(Color.pink);
			jpQuestions[i].setLayout(null);
			labQuestions[i] = new JLabel(questions[i], JLabel.TRAILING);
			labQuestions[i].setFont(new Font("Consolas", 0, 30));
			jpQuestions[i].add(labQuestions[i]);
			labQuestions[i].setBounds(0, 0, 400, 50);
			tfdAnswer[i] = new JTextField(8);
			tfdAnswer[i].setFont(new Font("Consolas", 0, 12));
			tfdAnswer[i].setBackground(Color.white);
			jpQuestions[i].add(tfdAnswer[i]);
			jpQuestions[i].setFont(new Font("MS UI Gothic", Font.PLAIN, 200));
			tfdAnswer[i].setBounds(410, 10, 60, 30);
			jpTest.add(jpQuestions[i]);
		}
		jpMain.add(jpTest).setBounds(100, 60, 500, 400);
		add(jpMain);
	}

	// 显示答题时间
	public void showTime()
	{
		labTime = new JLabel(tips.get(1) + "00:00");
		labTime.setBounds(50, 0, 120, 50);
		jpMain.add(labTime);
		new Thread()
		{
			public void run()
			{
				while (true)
				{
					try
					{
						Thread.sleep(1000);
						seconds++;
						if (seconds >= 60)
						{
							seconds = 0;
							minutes++;
						}
						if (seconds < 10)
							secStr = "0" + seconds.toString();
						else
							secStr = seconds.toString();
						if (minutes < 10)
							minStr = "0" + minutes.toString();
						else
							minStr = minutes.toString();
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
					labTime.setText(tips.get(1) + minStr + ":" + secStr);
					if (isEnd)
						break;
				}
			}
		}.start();
	}

	// 设置客户端语言
	public void setLanguage()
	{
		String[] choiceLanguage =
		{ "简体中文", "繁体中文", "English" };
		String language = (String) JOptionPane.showInputDialog(null, "请选择客户端的语言:\n",
				"Choice a language for client", JOptionPane.PLAIN_MESSAGE,
				new ImageIcon("icon.png"), choiceLanguage, "简体中文");
		ButtonGroup g = new ButtonGroup();
		JRadioButton JRB1 = new JRadioButton("简体中文", true);
		JRadioButton JRB2 = new JRadioButton("繁体中文");
		JRadioButton JRB3 = new JRadioButton("English");
		g.add(JRB1);
		g.add(JRB2);
		g.add(JRB3);

		if (language == null)
		{
			System.exit(-1);
		} else
		{
			try
			{
				reader = new BufferedReader(new FileReader(new File(language + ".txt")));
				String s;
				while ((s = reader.readLine()) != null)
				{
					tips.add(s);
				}
				reader.close();
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		btnExit.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				// TODO Auto-generated method stub
				System.exit(0);
			}
		});

		btnSubmit.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent arg0)
			{
				// TODO Auto-generated method stub
				isEnd = !isEnd;
				for (int i = 0; i < Createformula.testNum; i++)
				{
					answers[i] = tfdAnswer[i].getText();
					// System.out.println(answers[i]);
				}
				wrong = background.checkAnswer(answers);
				String s = null;
				if (wrong.length == 0)
					s = tips.get(5);
				else
				{
					s = tips.get(6) + "\n";
					String standardAnswer[] = new String[Createformula.testNum];
					standardAnswer = background.getStandardAnswer();
					questions = background.getQuestions();
					for (int i = 0; i < wrong.length; i++)
					{
						s = s + questions[new Integer(wrong[i]) - 1]
								+ standardAnswer[new Integer(wrong[i]) - 1];
						s = s + "\n";

					}
					double Accuracy = (Createformula.testNum - wrong.length) * 1.0
							/ Createformula.testNum * 100;
					s += "\n\n\n" + tips.get(2) + Accuracy + "%";
				}

				JOptionPane.showMessageDialog(null, s, tips.get(7), JOptionPane.PLAIN_MESSAGE);
			}

		});

	}
}
