package t1;

import t1.Fraction;

public class Calculator
{
	public static String add(Fraction a, Fraction b) // 加法
	{
		int m = a.getNumerator() * b.getDenominator() + a.getDenominator() * b.getNumerator();
		int n = a.getDenominator() * b.getDenominator();
		int t = Fraction.GCD(m, n);
		return Fraction.Reduction(m / t, n / t);
	}

	public static String sub(Fraction a, Fraction b)// 减法
	{
		int m = a.getNumerator() * b.getDenominator() - a.getDenominator() * b.getNumerator();
		int n = a.getDenominator() * b.getDenominator();
		int t = Fraction.GCD(m, n);
		return Fraction.Reduction(m / t, n / t);//
	}

	public static String mul(Fraction a, Fraction b)// 乘法
	{
		int m = a.getNumerator() * b.getNumerator();
		int n = a.getDenominator() * b.getDenominator();
		int t = Fraction.GCD(m, n);
		return Fraction.Reduction(m / t, n / t);
	}

	public static String div(Fraction a, Fraction b)// 除法
	{
		int m = a.getNumerator() * b.getDenominator();
		int n = a.getDenominator() * b.getNumerator();
		int t = Fraction.GCD(m, n);
		return Fraction.Reduction(m / t, n / t);
	}
}
